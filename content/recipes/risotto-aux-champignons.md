+++
title = "risotto aux champignons"
date = 2019-10-20T21:18:00+02:00
tags = ["recipe"]
seasons = ["autumn"]
ingredients = ["abricot", "cardamome", "gingembre"]
description = "Nectar d'abricot parfumé à la cardamome"
+++

Sans gluten
Vegan si vous liez votre risotto avec de la purée d’amandes

## Préparation : 15 minutes

## Cuisson : 20 minutes

## Ingrédients (pour 4 personnes)

- 200 g de riz spécial risotto (à grains ronds type carnaroli)
- 300 g de champignons de Paris frais
- 2 belles carottes
- 1 branche de céleri
- 1 oignon
- ½ bouquet de ciboulette
- 2 c. à soupe de Bouillon Le Classique
- 60 cl d’eau bouillante
- 5 cl de vin blanc (facultatif)
- 2 c. à soupe d’huile d’olive
- Sel fin, poivre du moulin
- Pour lier le risotto, au choix : 50 g de parmesan fraîchement râpé ou 1 c. à soupe de purée d’amandes blanches


## Réalisation

- Pelez les carottes, le céleri et l’oignon. Émincez-les finement. Rincez et séchez les champignons. Coupez-les en lamelles.
- Préparez un bouillon en mélangeant l’eau chaude et le bouillon en poudre.
- Faites chauffer l’huile d’olive dans une sauteuse et faites-y dorer les carottes, le céleri et l’oignon.
- Ajoutez le riz et mélangez pour l’enrober d’huile : le grain doit devenir légèrement transparent. Ajoutez les champignons, puis versez le vin et laissez-le s’évaporer.
- Ajoutez la moitié du bouillon. Mélangez régulièrement en laissant le bouillon s’évaporer.
- Ajoutez le bouillon restant et mélangez régulièrement jusqu’à ce que le riz soit cuit.
- Hors du feu, ajoutez le parmesan ou la purée d’amande. Mélangez vivement pour obtenir une texture crémeuse.
- Hachez finement la ciboulette. Saupoudrez-en le risotto.
- Goûtez, salez, poivrez et servez sans attendre.
