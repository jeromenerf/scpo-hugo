+++
title = "Sang Fezi"
date = 2019-11-05T13:58:32+01:00
tags = ["hot", "spicy", "sweet", "sour", "salty", "nasty"]
artist = "Wycleff Jean"
seasons = ["spring", "summer", "autumn"]
youtube = "VdI4tjOMekA"
+++

## Lyrics

```
[Chorus: Wyclef Jean]
Ki ayisien kap di'm map mache New-York san fezi
Mwen di ou messie nou menti
Le bum yo ginbe ou yo devore ou se le ou mouri police vini

[Verse 1: Wyclef Jean]
Penden'm kanpe sou flatbush mwen woue on police ap vini
Li di mwen map vann drog
Mwen di mon che ou menti
Li vin pou'l vin cheke'm mwen di mwen pa gen anyen
Se nan yon ti bal mwen tapwal la pou' cheke on ti zen
Big up big up phantom mwen renmen Tabou Combo
Se sak fe depwim nan bal la mwen mande kote moun yo
Timtim bwa sek mwen couri lem ouvri pot
Nan yon cimitie pase mwen woue on tet san kok
A b c d f g si`ou vle batay se danje
Mem si ou kon jwe foutbol denie gol lan se pou Pelé
Pa joure manmam, manmam se kreol
Fesi'm se bousol
Kompa'm se lakol
Original ayisieeennnn kwa de boukeeee
Gen neg ki pense brooklyn'm fet ayiti'm fet kwa de bouke

[Chorus: Wyclef Jean]
Ki ayisien kap di'm map mache New-York san fezi
Mwen di ou messie nou menti
Le bum yo ginbe ou yo devore ou se le ou mouri police vini

[Verse 2: Wyclef Jean]
Prezidan, mwen pa vle prezidan ou
Wase mikrofon nan mwen se gouveman ou
Bon nom'm se Wyclef yo rele'm fanfan
Mwen gen on ti se yo rele Roz Salon
Mwen yon cheval ki vole chak swa
Mwen gen cheve pwav, mwen pa vle cheve soi
Bad boy Ayisien ki soti en Ayiti
Depui mwen gen pwoblem mwen rale ouzi
Le mal lekol ameriken te kon joure'm
Yo relem neg nwe, yo relem ti refijie
Jan yo pale mwen woue yo pa civilise
Jan yo pale mwen woue yo pa kon bon Djie
45 bo kotem plis mwen gen de pie kole
Si on neg teste'm mape pete de gren zie'l
Orijinal ayitien ki soti jerizalem
Si on neg teste'm ane sa a ou pap oue nwel

[Chorus: Wyclef Jean]
Ki ayisien kap di'm map mache New-York san fezi
Mwen di ou messie nou menti
Le bum yo ginbe ou yo devore ou se le ou mouri police vini

[Interlude: Wyclef Jean]
Kote moun yo? mwen pa woue moun yo

[Bridge: Lauryn Hill]
Then you should know
That one day we are gone
So keep your head to the sky
See the path we refuse is the path we should choose
They won't take the world when you die

[Outro: Wyclef Jean]
Ayitien tiree
Leve le men, leve le men, chante, chante
Yon bagay yo di'm fi petyon vil pa vle neg ki gen cheve pwav
Mwen gen cheve pwav
```
